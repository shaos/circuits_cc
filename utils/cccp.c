/*
    cccp.c - Circuits.CC processing tool for validation and conversion

    Copyright (C) 2019 Alexander A. Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
#define DEBUG
*/
#ifndef NOSILK
#include "nedofont.c"
#endif

#define DX 256
#define DY 256

#define CCC_RULE_VALMASK   0x0000FFFF

#define CCC_RULE_EMPTY     0x00010000
#define CCC_RULE_SILK      0x00020000
#define CCC_RULE_VIA       0x00040000
#define CCC_RULE_DRILL     0x00080000
#define CCC_RULE_TOPSOLDER 0x00100000
#define CCC_RULE_BOTSOLDER 0x00200000
#define CCC_RULE_TOPCOPPER 0x00400000
#define CCC_RULE_BOTCOPPER 0x00800000
#define CCC_RULE_TOPRIGHT  0x01000000
#define CCC_RULE_TOPUP     0x02000000
#define CCC_RULE_TOPLEFT   0x04000000
#define CCC_RULE_TOPDOWN   0x08000000
#define CCC_RULE_BOTRIGHT  0x10000000
#define CCC_RULE_BOTUP     0x20000000
#define CCC_RULE_BOTLEFT   0x40000000
#define CCC_RULE_BOTDOWN   0x80000000

#define CCC_RULE_BOTHSOLDER (CCC_RULE_TOPSOLDER|CCC_RULE_BOTSOLDER)
#define CCC_RULE_TOPALL (CCC_RULE_TOPRIGHT|CCC_RULE_TOPUP|CCC_RULE_TOPLEFT|CCC_RULE_TOPDOWN)
#define CCC_RULE_BOTALL (CCC_RULE_BOTRIGHT|CCC_RULE_BOTUP|CCC_RULE_BOTLEFT|CCC_RULE_BOTDOWN)

#define RULES 256

unsigned int rules[RULES]; // 256 32-bit rules

char ccc[256];

int mode = 'V';
int plane_x = 0;
int plane_y = 0;
int plane_max = 0;
unsigned **plane;
#ifndef NOSILK
unsigned silk_offset[256];
#define SILKLINES 2000
unsigned short silk_lines[SILKLINES];
#define SILKX1(s) ((s)&15)
#define SILKY1(s) (((s)>>4)&15)
#define SILKX2(s) (((s)>>8)&15)
#define SILKY2(s) (((s)>>12)&15)
#endif

int addchar(int c)
{
  static int oldc = -1;
  if(c==0 && oldc==0) return 0;
#if 0
  if(c==0) printf("\n");
  else if(c>=32 && c<127) printf("%c",c);
  else printf(".");
#endif
  oldc = c;
  if(oldc)
  {
    plane[plane_y][plane_x++] = rules[oldc]|oldc;
    if(plane_x > plane_max) plane_max = plane_x;
  }
  else
  {
    plane_x=0;
    plane_y++;
  }
  return 1;
}

FILE* fopenGE(char* suffix)
{
 FILE* f;
 char str[256];
 int outline = 0;
 int soldermask = 0;
 int silkscreen = 0;
 if(!strcmp(suffix,".GTO")) silkscreen = 1;
 if(!strcmp(suffix,".GKO")) outline = 1;
 if(suffix[3]=='S') soldermask = 1;
 strncpy(str,ccc,256);
 strncat(str,suffix,256);
 str[255] = 0;
 printf("%s\n",str);
 f = fopen(str,"wt");
 if(f!=NULL)
 {
   fprintf(f,"G75*\n");
   fprintf(f,"G70*\n");
   fprintf(f,"%%OFA0B0*%%\n");
   fprintf(f,"%%FSLAX24Y24*%%\n");
   fprintf(f,"%%IPPOS*%%\n");
   fprintf(f,"%%LPD*%%\n");
   fprintf(f,"%%AMOC8*\n");
   fprintf(f,"5,1,8,0,0,1.08239X$1,22.5*\n");
   fprintf(f,"%%\n");
   if(silkscreen) fprintf(f,"%%ADD10C,0.0070*%%\n"); /* D10* for silkscreen layer */
   else if(outline) fprintf(f,"%%ADD10C,0.0000*%%\n"); /* D10* for outline layer */
   else
   { /* for copper layers and soldermask layers: */
     fprintf(f,"%%ADD10OC8,0.06%c0*%%\n",soldermask?'8':'0'); /* D10* octagonal pad */
     fprintf(f,"%%ADD11C,0.06%c0*%%\n",soldermask?'8':'0'); /* D11* wide oval pad */
     if(soldermask) fprintf(f,"%%ADD12R,0.0580X0.0580*%%\n");
     else fprintf(f,"%%ADD12R,0.0510X0.0510*%%\n"); /* D12* square pad */
     if(soldermask) fprintf(f,"%%ADD13R,0.0880X0.0340*%%\n");
     else fprintf(f,"%%ADD13R,0.0800X0.0260*%%\n"); /* D13* surface mount pad */
     fprintf(f,"%%ADD14C,0.0357*%%\n"); /* D14* circular via */
     fprintf(f,"%%ADD15C,0.0120*%%\n"); /* D15* signal trace */
     fprintf(f,"%%ADD16C,0.0500*%%\n"); /* D16* power trace */
   }
 }
 return f;
}

int fcloseGE(FILE *f)
{
 if(f==NULL) return 0;
 fprintf(f,"M02*\n");
 fclose(f);
 return 1;
}

FILE* fopenEX(char *suffix)
{
 FILE* f;
 char str[256];
 strncpy(str,ccc,256);
 strncat(str,suffix,256);
 str[255] = 0;
 printf("%s\n",str);
 f = fopen(str,"wt");
 if(f!=NULL)
 {
   fprintf(f,"%%\n");
   fprintf(f,"M48\n");
   fprintf(f,"M72\n");
   fprintf(f,"T01C0.0197\n"); // T01 - small drill
   fprintf(f,"T02C0.0400\n"); // T02 - big drill
   fprintf(f,"%%\n");
 }
 return f;
}

int fcloseEX(FILE *f)
{
 if(f==NULL) return 0;
 fprintf(f,"M30\n");
 fclose(f);
 return 1;
}

int checkEX(FILE *f, int x, int y)
{
 if(f==NULL) return 0;
 fprintf(f,"X%04dY%04d\n",x,y);
 return 1;
}

int main(int argc, char **argv)
{
 int c,i,j,k,kk,kkk,xx1,xx2,yy1,yy2,xxx,yyy,yyyy;
 char s[8],str[256],*po;
 FILE *f,*fT,*fB,*fTs,*fBs;

 printf("\ncccp v0.1 - Circuits.CC processing tool for validation and conversion\n\n");
 printf("Copyright (C) 2019 Alexander A. Shabarshin <me@shaos.net>\n\n");

 if(argc < 2)
 {
    printf("Usage:\n\tcccp filename.ccc [COMMAND]\n\n");
    printf("where COMMAND could be:\n\n");
    printf("\tVALIDATE - to validate input CCC-file (default command)\n");
    printf("\tGERBERS  - to convert input CCC-file to Gerbers & Excellon\n");
    printf("\tHTML     - to convert input CCC-file to HTML file\n");
    printf("\tTEXT     - to convert input CCC-file to text (stdout)\n");
    printf("\n");
    return -1;
 }
 strncpy(ccc,argv[1],256);
 ccc[255] = 0;
 if(argc > 2)
 {
    mode = argv[2][0];
    if(mode!='V' && mode!='G' && mode!='H' && mode!='T')
    {
       printf("ERROR: Wrong mode '%s'\n",mode);
       return -2;
    }
 }
 f = fopen(ccc,"rt");
 if(f==NULL)
 {
    printf("ERROR: Can't open file '%s'\n",ccc);
    return -3;
 }

 plane = (unsigned**)malloc(sizeof(unsigned*)*DY);
 if(plane==NULL) return -1000;
 for(j=0;j<DY;j++)
 {
   plane[j] = (unsigned*)malloc(sizeof(unsigned)*DX);
   if(plane[j]==NULL) return -1001;
   for(i=0;i<DX;i++) plane[j][i] = CCC_RULE_EMPTY;
 }

 for(i=0;i<RULES;i++)
 {
   rules[i] = i;
   if(i>=0x0E && i<=0xDA) rules[i] |= CCC_RULE_SILK;
   if(i>=0xE0 && i<=0xFF) rules[i] |= CCC_RULE_SILK;
 }
 rules[0x00] |= CCC_RULE_EMPTY;
 rules[0x01] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTALL|CCC_RULE_TOPLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x02] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTALL|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x03] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTALL|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x04] |= CCC_RULE_BOTALL|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x05] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_TOPLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x06] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x07] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x08] |= CCC_RULE_BOTLEFT|CCC_RULE_BOTDOWN|CCC_RULE_BOTCOPPER;
 rules[0x09] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTDOWN|CCC_RULE_TOPLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x0A] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTDOWN|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x0B] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTDOWN|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x0C] |= CCC_RULE_BOTRIGHT|CCC_RULE_BOTUP|CCC_RULE_BOTCOPPER;
 rules[0x0D] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_BOTDOWN|CCC_RULE_TOPLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x0E] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_BOTDOWN|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x0F] |= CCC_RULE_BOTHSOLDER|CCC_RULE_VIA|CCC_RULE_BOTUP|CCC_RULE_BOTDOWN|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x10] |= CCC_RULE_BOTALL|CCC_RULE_BOTCOPPER;
 rules[0x11] |= CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER;
 rules[0x12] |= CCC_RULE_BOTUP|CCC_RULE_BOTDOWN|CCC_RULE_BOTCOPPER;
 rules[0x13] |= CCC_RULE_BOTUP|CCC_RULE_BOTDOWN|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x14] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPRIGHT|CCC_RULE_BOTRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x15] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_BOTRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x16] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPLEFT|CCC_RULE_BOTLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x17] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_BOTLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x18] |= CCC_RULE_BOTHSOLDER|CCC_RULE_DRILL|CCC_RULE_TOPALL|CCC_RULE_BOTALL|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x19] |= CCC_RULE_BOTHSOLDER|CCC_RULE_DRILL|CCC_RULE_TOPALL|CCC_RULE_BOTALL|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0x1A] |= CCC_RULE_TOPSOLDER|CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER;
 rules[0x1B] |= CCC_RULE_TOPSOLDER|CCC_RULE_TOPALL|CCC_RULE_TOPCOPPER;
 rules[0x1C] |= CCC_RULE_TOPLEFT|CCC_RULE_TOPUP|CCC_RULE_TOPCOPPER;
 rules[0x1D] |= CCC_RULE_TOPDOWN|CCC_RULE_TOPRIGHT|CCC_RULE_TOPCOPPER;
 rules[0x20] |= CCC_RULE_EMPTY;
 rules[0xDB] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPALL|CCC_RULE_BOTALL|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;;
 rules[0xDC] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPLEFT|CCC_RULE_TOPDOWN|CCC_RULE_TOPRIGHT|CCC_RULE_BOTLEFT|CCC_RULE_BOTDOWN|CCC_RULE_BOTRIGHT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0xDD] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPUP|CCC_RULE_TOPLEFT|CCC_RULE_TOPDOWN|CCC_RULE_BOTUP|CCC_RULE_BOTLEFT|CCC_RULE_BOTDOWN|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0xDE] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPDOWN|CCC_RULE_TOPRIGHT|CCC_RULE_TOPUP|CCC_RULE_BOTDOWN|CCC_RULE_BOTRIGHT|CCC_RULE_BOTUP|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;
 rules[0xDF] |= CCC_RULE_BOTHSOLDER|CCC_RULE_TOPRIGHT|CCC_RULE_TOPUP|CCC_RULE_TOPLEFT|CCC_RULE_BOTRIGHT|CCC_RULE_BOTUP|CCC_RULE_BOTLEFT|CCC_RULE_TOPCOPPER|CCC_RULE_BOTCOPPER;

#ifndef NOSILK
 int ch[8][8];
 kk = 0;
 silk_lines[kk++] = 0xFFFF;
 for(i=0;i<256;i++)
 {
   if(i<33 || (i>=0xDB && i<=0xDF))
   {
      silk_offset[i] = 0;
      continue;
   }
   silk_offset[i] = kk;
   for(j=0;j<8;j++)
   {
      c = font8x8[i-32][7-j];
      for(k=0;k<8;k++)
      {
         if(c&(1<<(7-k)))
           ch[k][j] = 1;
         else
           ch[k][j] = 0;
      }
   }
   c = 1;
   while(c)
   {
     xx1 = xx2 = yy1 = yy2 = -1;
     xxx = 0;
     for(j=0;j<8;j++)
     {
        kkk = yyy = yyyy = 0;
        for(k=0;k<=8;k++)
        {
          if(k<8 && (ch[k][j]<0 || ch[k][j]>0))
          {
             if(!yyyy) yyy = k;
             if(ch[k][j]>0) kkk++;
             yyyy++;
          }
          else
          {
             if(kkk && yyyy > xxx)
             {
                xx1 = yyy;
                xx2 = yyy + yyyy - 1;
                yy1 = yy2 = j;
                xxx = yyyy;
//                printf("Length %i X=%i..%i Y=%i\n",yyyy,xx1,xx2,j);
             }
             kkk = yyyy = 0;
          }
        }
     }
     for(k=0;k<8;k++)
     {
        kkk = yyy = yyyy = 0;
        for(j=0;j<=8;j++)
        {
          if(j<8 && (ch[k][j]<0 || ch[k][j]>0))
          {
             if(!yyyy) yyy = j;
             if(ch[k][j]>0) kkk++;
             yyyy++;
          }
          else
          {
             if(kkk && yyyy > xxx)
             {
                yy1 = yyy;
                yy2 = yyy + yyyy - 1;
                xx1 = xx2 = k;
                xxx = yyyy;
//                printf("Length %i X=%i Y=%i..%i\n",yyyy,k,yy1,yy2);
             }
             kkk = yyyy = 0;
          }
        }
     }

     if(xxx)
     {
//        printf("%2.2X LINE %i:%i-%i:%i (%i)\n\n",i,xx1,yy1,xx2,yy2,xxx);
        silk_lines[kk++] = xx1|(yy1<<4)|(xx2<<8)|(yy2<<12);
//        printf("%2.2X %i 0x%4.4X\n",i,kk-1,silk_lines[kk-1]);
     }

     if(xx1>=0 && xx1==xx2)
       for(yyy=yy1;yyy<=yy2;yyy++)
           ch[xx1][yyy] = -1;
     if(yy1>=0 && yy1==yy2)
       for(yyy=xx1;yyy<=xx2;yyy++)
           ch[yyy][yy1] = -1;
     c = 0;
     for(k=0;k<8;k++)
     {
       for(j=0;j<8;j++)
       {
          if(ch[k][j]>0) c++;
       }
     }
   }
   silk_lines[kk++] = 0xFFFF;
   if(kk >= SILKLINES-64)
   {
      printf("ERROR: Too many lines %i\n",kk);
      free(plane);
      fclose(f);
      return -1;
   }
 }
#endif

 j = 0;
 while(1)
 {
   c = fgetc(f);
   if(c==',')
   {
      s[j] = 0;
      k = atoi(s);
      if(!addchar(k)) break;
      j = 0;
      continue;
   }
   s[j++] = c;
   if(j==7)
   {
      s[7] = 0;
      printf("ERROR: Invalid value %s\n",s);
      free(plane);
      fclose(f);
      return -3;
   }

   if(c=='\r' || c=='\n') continue;

   if(feof(f)) break;
 }
 s[j] = 0;
 k = atoi(s);
 addchar(k);

 fclose(f);

 po = strstr(ccc,".ccc");
 if(po!=NULL) *po = 0;
 po = strstr(ccc,".CCC");
 if(po!=NULL) *po = 0;

 /* ANALYSIS */

 for(j=0;j<plane_y;j++)
 {
    c = ' ';
    for(i=0;i<plane_max;i++)
    {
       c = plane[j][i] & CCC_RULE_VALMASK;
       if(c > 0x20 && c < 0x80 && i > 0)
       {
          if(plane[j][i-1] & CCC_RULE_TOPRIGHT)
          {
             plane[j][i] |= CCC_RULE_TOPLEFT|CCC_RULE_TOPRIGHT;
          }
       }
    }
 }

 if(mode=='G') /* GERBERS */
 {

/*
   pcbname.GTL - Top Copper
   pcbname.GTS - Top Soldermask
   pcbname.GTO - Top Silkscreen
   pcbname.GBL - Bottom Copper
   pcbname.GBS - Bottom Soldermask
   pcbname.XLN - Drills
   pcbname.GKO - Board Outline
*/

  /* DRILLS */

  f = fopenEX(".XLN");
  if(f!=NULL)
  {
   fprintf(f,"T01\n");

   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      if(j&1) /* from right to left */
      {
         for(i=plane_max;i>=0;i--)
           if(plane[j][i] & CCC_RULE_VIA)
             checkEX(f,500*i+500,yyy);
      }
      else /* from left to right */
      {
         for(i=0;i<=plane_max;i++)
           if(plane[j][i] & CCC_RULE_VIA)
             checkEX(f,500*i+500,yyy);
      }
      yyy += 500;
   }

   fprintf(f,"T02\n");

   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      if(j&1) /* from right to left */
      {
         for(i=plane_max;i>=0;i--)
           if(plane[j][i] & CCC_RULE_DRILL)
             checkEX(f,500*i+500,yyy);
      }
      else /* from left to right */
      {
         for(i=0;i<=plane_max;i++)
           if(plane[j][i] & CCC_RULE_DRILL)
             checkEX(f,500*i+500,yyy);
      }
      yyy += 500;
   }

   fcloseEX(f);
  }

  /* COPPER */

  fT = fopenGE(".GTL");
  fB = fopenGE(".GBL");

  /* SOLDERMASK */

  fTs = fopenGE(".GTS");
  fBs = fopenGE(".GBS");

  if(fT!=NULL && fB!=NULL && fTs!=NULL && fBs!=NULL)
  {
   /* octagonal pads */

   fprintf(fT,"D10*\n");
   fprintf(fB,"D10*\n");
   fprintf(fTs,"D10*\n");
   fprintf(fBs,"D10*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         if((plane[j][i] & CCC_RULE_VALMASK)==0x19)
         {
            sprintf(str,"X%06dY%06dD03*\n",500*i+500,yyy);
            fprintf(fT,str);
            fprintf(fB,str);
            fprintf(fTs,str);
            fprintf(fBs,str);
         }
      }
      yyy += 500;
   }

   /* wide oval pads */

   fprintf(fT,"D11*\n");
   fprintf(fB,"D11*\n");
   fprintf(fTs,"D11*\n");
   fprintf(fBs,"D11*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         if(i>0 && (plane[j][i] & CCC_RULE_VALMASK)==0x18 &&
            (((plane[j][i-1] & CCC_RULE_VALMASK)==0x14)||((plane[j][i-1] & CCC_RULE_VALMASK)==0x15)) &&
            (((plane[j][i+1] & CCC_RULE_VALMASK)==0x16)||((plane[j][i+1] & CCC_RULE_VALMASK)==0x17)) )
         {
            sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",500*i+200,yyy,500*i+800,yyy);
            fprintf(fT,str);
            fprintf(fB,str);
            fprintf(fTs,str);
            fprintf(fBs,str);
         }
      }
      yyy += 500;
   }

   /* square pads */

   fprintf(fT,"D12*\n");
   fprintf(fB,"D12*\n");
   fprintf(fTs,"D12*\n");
   fprintf(fBs,"D12*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         c = plane[j][i] & CCC_RULE_VALMASK;
         if(i>0 && (c==0x18||c==0x1B||c==0xDB) &&
            ((plane[j][i-1] & CCC_RULE_VALMASK)!=0x14) && ((plane[j][i-1] & CCC_RULE_VALMASK)!=0x15) &&
            ((plane[j][i+1] & CCC_RULE_VALMASK)!=0x16) && ((plane[j][i+1] & CCC_RULE_VALMASK)!=0x17) )
         {
            sprintf(str,"X%06dY%06dD03*\n",500*i+500,yyy);
            fprintf(fT,str);
            if(c!=0x1B) fprintf(fB,str);
            fprintf(fTs,str);
            if(c!=0x1B) fprintf(fBs,str);
         }
      }
      yyy += 500;
   }

   /* surface mount pads */

   fprintf(fT,"D13*\n");
   fprintf(fTs,"D13*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         if((plane[j][i] & CCC_RULE_VALMASK)==0x1A)
         {
            sprintf(str,"X%06dY%06dD03*\n",500*i+500,yyy);
            fprintf(fT,str);
            fprintf(fTs,str);
         }
      }
      yyy += 500;
   }

   /* circular vias */

   fprintf(fT,"D14*\n");
   fprintf(fB,"D14*\n");
   fprintf(fTs,"D14*\n");
   fprintf(fBs,"D14*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         if(plane[j][i] & CCC_RULE_VIA)
         {
            sprintf(str,"X%06dY%06dD03*\n",500*i+500,yyy);
            fprintf(fT,str);
            fprintf(fTs,str);
            fprintf(fB,str);
            fprintf(fBs,str);
         }
      }
      yyy += 500;
   }

   /* signal trace */

   fprintf(fT,"D15*\n");
   fprintf(fB,"D15*\n");
   yyy = 0;
   for(j=plane_y;j>=0;j--)
   {
      xxx = 0;
      for(i=0;i<=plane_max;i++)
      {
         if((plane[j][i] & CCC_RULE_VALMASK)==0x1C)
         {
            if(xxx)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\nX%06dY%06dD01*\n",xxx,yyy,500*i+250,yyy,500*i+500,yyy+250);
              fprintf(fT,str);
              xxx = 0;
            }
            continue;
         }
         if(xxx && (plane[j][i] & CCC_RULE_TOPLEFT))
         {
            if(plane[j][i] & CCC_RULE_TOPRIGHT) continue;
            else
            {
               sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy,500*i+500,yyy);
               fprintf(fT,str);
               xxx = 0;
            }
         }
         if(xxx && !(plane[j][i] & CCC_RULE_TOPLEFT))
         {
            if(k!=i-1)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy,500*i,yyy);
              fprintf(fT,str);
            }
            xxx = 0;
         }
         if(!xxx && (plane[j][i] & CCC_RULE_TOPRIGHT))
         {
            k = i;
            xxx = 500*i+500;
            if((plane[j][i] & CCC_RULE_VALMASK)==0x1D)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy-250,500*i+750,yyy);
              fprintf(fT,str);
              xxx += 250;
            }
            continue;
         }
      }
      yyy += 500;
   }

   xxx = 500;
   for(i=0;i<=plane_max;i++)
   {
      yyyy = yyy = 0;
      for(j=plane_y;j>=0;j--,yyy+=500)
      {
         if((plane[j][i] & CCC_RULE_VALMASK)==0x08)
         {
            if(yyyy)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\nX%06dY%06dD01*\n",xxx,yyyy,xxx,yyy-250,xxx-250,yyy);
              fprintf(fB,str);
              yyyy = 0;
            }
            continue;
         }
         if(yyyy && (plane[j][i] & CCC_RULE_BOTDOWN))
         {
            if(plane[j][i] & CCC_RULE_BOTUP) continue;
            else
            {
               sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy,xxx,yyyy);
               fprintf(fB,str);
               yyyy = 0;
            }
         }
         if(yyyy && !(plane[j][i] & CCC_RULE_BOTDOWN))
         {
            if(k!=j+1)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy-500,xxx,yyyy);
              fprintf(fB,str);
            }
            yyyy = 0;
         }
         if(!yyyy && (plane[j][i] & CCC_RULE_BOTUP))
         {
            k = j;
            yyyy = yyy;
            if((plane[j][i] & CCC_RULE_VALMASK)==0x0C)
            {
              sprintf(str,"X%06dY%06dD02*\nX%06dY%06dD01*\n",xxx,yyy+250,xxx+250,yyy);
              fprintf(fB,str);
              yyyy += 250;
            }
            continue;
         }
      }
      xxx += 500;
   }

  }

  if(fT!=NULL) fcloseGE(fT);
  if(fB!=NULL) fcloseGE(fB);
  if(fTs!=NULL) fcloseGE(fTs);
  if(fBs!=NULL) fcloseGE(fBs);

  /* SILKSCREEN */

  f = fopenGE(".GTO");
  if(f!=NULL)
  {
#ifndef NOSLIK
   fprintf(f,"D10*\n");
   yyy = -220;
   for(j=plane_y;j>=0;j--)
   {
      for(i=0;i<=plane_max;i++)
      {
         if(plane[j][i] & CCC_RULE_SILK)
         {
            k = silk_offset[plane[j][i] & 255];
            while(silk_lines[k]!=0xFFFF)
            {
               c = silk_lines[k];
               xx1 = SILKX1(c);
               yy1 = SILKY1(c);
               xx2 = SILKX2(c);
               yy2 = SILKY2(c);
               if(xx1==xx2 && yy1==yy2)
                  fprintf(f,"X%06dY%06dD03*\n",
                     500*i+280+xx1*62,yyy+yy1*62);
               else
                  fprintf(f,"X%06dY%06dD02*\nX%06dY%06dD01*\n",
                     500*i+280+xx1*62,yyy+yy1*62,
                     500*i+280+xx2*62,yyy+yy2*62);
               k++;
            }
         }
      }
      yyy += 500;
   }
#endif
   fcloseGE(f);
  }

  /* OUTLINE */

  f = fopenGE(".GKO");
  if(f!=NULL)
  {
   fprintf(f,"D10*\n");
   xx1 = 0;
   yy1 = 0;
   xx2 = plane_max*500+500;
   yy2 = plane_y*500+500;
   fprintf(f,"X%06dY%06dD02*\n",xx1,yy1);
   fprintf(f,"X%06dY%06dD01*\n",xx1,yy2);
   fprintf(f,"X%06dY%06dD01*\n",xx2,yy2);
   fprintf(f,"X%06dY%06dD01*\n",xx2,yy1);
   fprintf(f,"X%06dY%06dD01*\n",xx1,yy1);
   fcloseGE(f);
  }

 }

 /* closing everything */

 for(j=0;j<DY;j++)
 {
#ifdef DEBUG
    if(j < plane_y)
    {
       printf("%03d ",j);
       for(i=0;i<plane_max;i++)
       {
          c = plane[j][i] & CCC_RULE_VALMASK;
          printf("%c",(c<32)?'.':((c>=128)?'#':c));
       }
       printf("\n");
    }
#endif
    free(plane[j]);
 }
 free(plane);

 return 0;
}


