#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char **argv)
{
 int i,sz,ft=0;
 unsigned char b;
 char str[100],*po;
 FILE *fi,*fo;
 if(argc>2 && argv[2][0]=='t') ft=1;
 fi = fopen(argv[1],"rb");
 if(fi==NULL) return -1;
 strcpy(str,argv[1]);
 po = strrchr(str,'.');
 if(po!=NULL) *po = 0;
 strcat(str,".txt");
 fo = fopen(str,"wt");
 if(fo==NULL)
 {
   fclose(fi);
   return -2;
 }
 fseek(fi,0,SEEK_END);
 sz = ftell(fi);
 fseek(fi,0,SEEK_SET);
 for(i=0;i<sz;i++)
 {
   b = fgetc(fi);
   if(ft)
   {
     if(b==0x0D) continue;
     if(b==0x0A) b=0;
   }
   fprintf(fo,"%i",b);
   if(i<sz-1) fprintf(fo,",");
 }
 fclose(fo);
 fclose(fi);
 return 0;
}
