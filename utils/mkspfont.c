/* Make nedoPC font for Sprinter computer 19-DEC-2020 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcbimgs.c"

int main()
{
 FILE *f;
 int i,j,k,b;
 long l,ptr=0;

 f = fopen("nedopc.bin","wb");
 if(f==NULL) return -1;
 for(i=0;i<256;i++)
 {
   for(j=0;j<8;j++)
   {
     b = 0;
     for(k=0;k<8;k++)
     {
         l = pcbimg[ptr++];
         b <<= 1;
         if(l & 0xFFFF00)
         {
            b |= 1;
         }
         else if(l & 0x0000FF)
         {
            b |= (j+k)&1;
         }
     }
     fputc(b,f);
   }
 }
 fclose(f);

 return 0;
}
