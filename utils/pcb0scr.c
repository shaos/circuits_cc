#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NOZEROS
#define COMMAS
#define PNG

#ifdef PNG
#include <png.h>
#include "pcbimgs.c"
#endif

int DX,DY,XOFF,YOFF;

unsigned char **scr;

#ifdef PNG
int PNGXOFF = 0;
int PNGYOFF = 0;
int PNGDX = -1;
int PNGDY = -1;
#endif
int marker = -1;

char filename[100] = "pcb0scr";

int main(int argc, char** argv)
{
 char str[100];
 char inpfile[100];
 char htmfile[100];
 char pngfile[100];
 char cccfile[100];
 int x,y,ix,iy,ic,it;
 int xmin = 1000000;
 int ymin = 1000000;
 int xmax = -1000000;
 int ymax = -1000000;
 int mxmin = xmin;
 int mxmax = xmax;
 int mymin = ymin;
 int mymax = ymax;
 sprintf(inpfile,"%s.out",filename);
 if(argc>1)
 {
    marker = atoi(argv[1]);
    printf("Marker=0x%2.2X\n",marker);
 }
 if(argc>2)
 {
    strcpy(filename,argv[2]);
    printf("Filename=%s\n",filename);
 }
 sprintf(htmfile,"%s.htm",filename);
 sprintf(pngfile,"%s.png",filename);
 sprintf(cccfile,"%s.ccc",filename);
 FILE *ff,*f = fopen(inpfile,"rt");
 if(f==NULL) return -1;
 while(1)
 {
   fgets(str,100,f);
   if(feof(f)) break;
#ifdef COMMAS
   sscanf(str,"%i,%i,%i,%i\n",&ix,&iy,&ic,&it);
#else
   sscanf(str,"%i|%i|%i|%i\n",&ix,&iy,&ic,&it);
#endif
   if(ix < xmin) xmin = ix;
   if(iy < ymin) ymin = iy;
   if(ix > xmax) xmax = ix;
   if(iy > ymax) ymax = iy;
   if(ic==marker)
   {
      if(ix < mxmin) mxmin = ix;
      if(iy < mymin) mymin = iy;
      if(ix > mxmax) mxmax = ix;
      if(iy > mymax) mymax = iy;
   }
 }
 printf("X=%i..%i\n",xmin,xmax);
 printf("Y=%i..%i\n",ymin,ymax);
 fclose(f);
 f = fopen(inpfile,"rt");
 if(f==NULL) return -1;
 DX = xmax-xmin+1;
 DY = ymax-ymin+1;
 XOFF = -xmin;
 YOFF = -ymin;
#ifdef PNG
 if(marker>=0)
 {
   PNGDX = mxmax-mxmin+1;
   PNGDY = mymax-mymin+1;
   PNGXOFF = XOFF+mxmin;
   PNGYOFF = YOFF+mymin;
   printf("MX=%i..%i\n",mxmin,mxmax);
   printf("MY=%i..%i\n",mymin,mymax);
 }
 else
 {
   PNGDX = DX;
   PNGDY = DY;
 }
 printf("PNGOFF = (%i,%i)\n",PNGXOFF,PNGYOFF);
 printf("PNGSZ = %i x %i\n",PNGDX<<3,PNGDY<<3);
 printf("BRDSZ = %2.2f x %2.2f\n",(PNGDX-1)*0.05,(PNGDY-1)*0.05);
#endif
 scr = (unsigned char**)malloc(DX*sizeof(unsigned char*));
 for(x=0;x<DX;x++)
 {
   scr[x] = (unsigned char*)malloc(DY);
   for(y=0;y<DY;y++) scr[x][y] = 0;
 }
 while(1)
 {
   fgets(str,100,f);
   if(feof(f)) break;
#ifdef COMMAS
   sscanf(str,"%i,%i,%i,%i\n",&ix,&iy,&ic,&it);
#else
   sscanf(str,"%i|%i|%i|%i\n",&ix,&iy,&ic,&it);
#endif
   x = ix + XOFF;
   y = iy + YOFF;
   if(x<0) x=0;
   if(x>=DX) x=DX-1;
   if(y<0) y=0;
   if(y>=DY) y=DY-1;
   scr[x][y] = ic;
 }
 fclose(f);

 f = fopen(htmfile,"wt");
 if(f==NULL) return -2;
 fprintf(f,"<table bgcolor=#000000 border=0 cellPadding=0 cellSpacing=0>");
 for(y=0;y<DY;y++){
 fprintf(f,"<tr>\n");
 for(x=0;x<DX;x++){
   sprintf(str,"pcb/%04x.gif",scr[x][y]);
   fprintf(f,"\t<td><img src=\"%s\"></td>\n",str);
 }
 fprintf(f,"</tr>\n");
 }
 fprintf(f,"</table>\n");
 fclose(f);
 printf("%s - done\n",htmfile);

#ifdef PNG
 int pngw = PNGDX<<3;
 int pngh = PNGDY<<3;
 f = fopen(pngfile, "wb");
 if(!f) return -3;
 ff = fopen(cccfile, "wt");
 if(!ff)
 {
    fclose(f);
    return -13;
 }
 png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
 if(!png_ptr) return -4;
 png_infop info_ptr = png_create_info_struct(png_ptr);
 if(!info_ptr) return -5;
 if(setjmp(png_jmpbuf(png_ptr))) return -6;
 png_init_io(png_ptr, f);
 if(setjmp(png_jmpbuf(png_ptr))) return -7;
 png_set_IHDR(png_ptr, info_ptr, pngw, pngh, 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
 png_write_info(png_ptr, info_ptr);
 if(setjmp(png_jmpbuf(png_ptr))) return -8;
 png_bytep* row_pointers = (png_bytep*) malloc(sizeof(png_bytep)*pngh);
 if(row_pointers==NULL) return -9;
 for(y=0;y<pngh;y++)
 {
    row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr,info_ptr));
    if(row_pointers[y]==NULL) return -10;
    iy = (y>>3)+PNGYOFF;
    for(x=0;x<pngw;x++)
    {
      ix = (x>>3)+PNGXOFF;
      if(!(x&7))
      {
         if(ix < 0 || ix >= DX || iy < 0 || iy >= DY) ic = 0;
         else ic = scr[ix][iy];
#ifdef NOZEROS
         if(ic==0) ic = ' '; // turn zeros to spaces
#endif
         if(ic==marker) ic = ' '; // make markers invisible
         if(!(y&7) && ( marker < 0 || ( x>0 && x<pngw-8 && y>0 && y<pngh-8 ) )) fprintf(ff,"%i,",ic);
      }
      it = pcbimg[(ic<<6) + (y&7)*8 + (x&7)];
      row_pointers[y][(x<<2)+0] = (it>>16)&255;
      row_pointers[y][(x<<2)+1] = (it>>8)&255;
      row_pointers[y][(x<<2)+2] = it&255;
      row_pointers[y][(x<<2)+3] = 255;
    }
    if(!(y&7) && ( marker < 0 || ( y>0 && y<pngh-8) )) fprintf(ff,"0,");
 }
 fprintf(ff,"0");
 png_write_image(png_ptr, row_pointers);
 if(setjmp(png_jmpbuf(png_ptr))) return -11;
 png_write_end(png_ptr, NULL);
 for(y=0;y<pngh;y++) free(row_pointers[y]);
 free(row_pointers);
 fclose(f);
 printf("%s - done\n",pngfile);
 fclose(ff);
 printf("%s - done\n",cccfile);
#endif

 for(x=0;x<DX;x++) free(scr[x]);
 free(scr);
 return 0;
}

