/*  font2gif.c - convert nedo-font 8x8 to set of gifs

    Part of NedoPC SDK (software development kit for simple devices)

    Copyright (C) 2012, Alexander A. Shabarshin <ashabarshin@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/*
    Compilation:
       gcc font2gif.c -o font2gif -lgif

    Usage:
       ./font2gif
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gif_lib.h>
#include "nedofont.c"

/* Gif-handling - thanks to Gershon Elber */

ColorMapObject *colormap = NULL;

static void QuitGifError(GifFileType *GifFile)
{
    PrintGifError();
    if (GifFile != NULL) EGifCloseFile(GifFile);
    exit(EXIT_FAILURE);
}

static void SaveGif(const char* FileName,
    GifByteType *OutputBuffer,
    ColorMapObject *OutputColorMap,
    int ExpColorMapSize, int Width, int Height)
{
    int i;
    GifFileType *GifFile;
    GifByteType *Ptr = OutputBuffer;

    if ((GifFile = EGifOpenFileName(FileName,0)) == NULL)
       QuitGifError(GifFile);

    if (EGifPutScreenDesc(GifFile,
          Width, Height, ExpColorMapSize, 0,
          OutputColorMap) == GIF_ERROR ||
        EGifPutImageDesc(GifFile,
          0, 0, Width, Height, FALSE, NULL) == GIF_ERROR)
       QuitGifError(GifFile);

    printf("Image '%s' at (%d, %d) [%dx%d]\n", FileName,
       GifFile->Image.Left, GifFile->Image.Top,
       GifFile->Image.Width, GifFile->Image.Height);

    for (i = 0; i < Height; i++) {
      if (EGifPutLine(GifFile, Ptr, Width) == GIF_ERROR)
         QuitGifError(GifFile);
      Ptr += Width;
    }

    if (EGifCloseFile(GifFile) == GIF_ERROR)
        QuitGifError(GifFile);
}

int main(int argc, char **argv)
{
 int i,j,k,l,c,h;
 char fname[16]="test.gif";
 unsigned char img[64];

 GifColorType ScratchMap2[2];
 GifColorType ScratchMap4[4];
 GifColorType ScratchMap[16];
#define SETCOL(x,y) {\
   ScratchMap[x].Red   = (y>>16)&0xFF;\
   ScratchMap[x].Green = (y>>8)&0xFF;\
   ScratchMap[x].Blue  = y&0xFF;\
   }

 SETCOL( 0,0x000000);
 SETCOL( 1,0x0000AA);
 SETCOL( 2,0x00AA00);
 SETCOL( 3,0x00AAAA);
 SETCOL( 4,0xAA0000);
 SETCOL( 5,0xAA00AA);
 SETCOL( 6,0xAA5500);
 SETCOL( 7,0xAAAAAA);
 SETCOL( 8,0x555555);
 SETCOL( 9,0x5555FF);
 SETCOL(10,0x55FF55);
 SETCOL(11,0x55FFFF);
 SETCOL(12,0xFF5555);
 SETCOL(13,0xFF55FF);
 SETCOL(14,0xFFFF55);
 SETCOL(15,0xFFFFFF);

 for(h=0;h<5;h++)
 {
  ScratchMap2[0] = ScratchMap[0];
  switch(h)
  {
    case 0: ScratchMap2[1] = ScratchMap[2]; break;
    case 1: ScratchMap2[1] = ScratchMap[7]; break;
    case 2: ScratchMap2[1] = ScratchMap[12]; break;
    case 3: ScratchMap2[1] = ScratchMap[11]; break;
    case 4: ScratchMap2[1] = ScratchMap[0];
            ScratchMap2[0] = ScratchMap[2]; break;
  }

  colormap = MakeMapObject(2,ScratchMap2);
  if(colormap==NULL) return -1;

  for(k=0;k<FONT8X8_CHARS;k++)
  {
   sprintf(fname,"0%i%2.2x.gif",h,k+FONT8X8_FIRST);
   l = 0;
   for(j=0;j<8;j++)
   {
     c = font8x8[k][j];
     for(i=0;i<8;i++)
     {
        if(c&0x80)
             img[l] = 1;
        else img[l] = 0;
        l++;c<<=1;
     }
   }
   SaveGif(fname,img,colormap,2,8,8);
  }
 }

#if 1 /* special PCB characters */

for(h=0;h<5;h++)
{
 if(h==0)
 {
   ScratchMap4[0] = ScratchMap[0];
   ScratchMap4[1] = ScratchMap[1];
   ScratchMap4[2] = ScratchMap[2];
   ScratchMap4[3] = ScratchMap[4];

   colormap = MakeMapObject(4,ScratchMap4);
   if(colormap==NULL) return -2;
 }
 if(h==1)
 {
   ScratchMap4[0] = ScratchMap[0];
   ScratchMap4[1] = ScratchMap[7];
   ScratchMap4[2] = ScratchMap[12];
   ScratchMap4[3] = ScratchMap[11];

   colormap = MakeMapObject(4,ScratchMap4);
   if(colormap==NULL) return -3;
 }
 if(h==4)
 {
   ScratchMap4[0] = ScratchMap[2];
   ScratchMap4[1] = ScratchMap[0];
   ScratchMap4[2] = ScratchMap[0];
   ScratchMap4[3] = ScratchMap[0];

   colormap = MakeMapObject(4,ScratchMap4);
   if(colormap==NULL) return -3;
 }

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[63]=2;
 }
 else
 {
  img[63]=h;
 }
 sprintf(fname,"0%i00.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++) img[i]=1;
  img[18]=2;img[19]=2;img[20]=2;img[21]=2;
  img[26]=2;img[27]=0;img[28]=0;img[29]=2;
  img[34]=2;img[35]=0;img[36]=0;img[37]=2;
  img[42]=2;img[43]=2;img[44]=2;img[45]=2;
  img[24]=3;img[25]=3;
  img[32]=3;img[33]=3;
 }
 else
 {
  for(i=0;i<64;i++) img[i]=h;
  img[18]=h;img[19]=h;img[20]=h;img[21]=h;
  img[26]=h;img[27]=0;img[28]=0;img[29]=h;
  img[34]=h;img[35]=0;img[36]=0;img[37]=h;
  img[42]=h;img[43]=h;img[44]=h;img[45]=h;
  img[24]=h;img[25]=h;
  img[32]=h;img[33]=h;
 }
 sprintf(fname,"0%i01.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[30]=3;img[31]=3;
  img[38]=3;img[39]=3;
 }
 else
 {
  img[30]=h;img[31]=h;
  img[38]=h;img[39]=h;
 }
 sprintf(fname,"0%i03.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[24]=1;img[25]=1;
  img[32]=1;img[33]=1;
 }
 else
 {
  img[24]=h;img[25]=h;
  img[32]=h;img[33]=h;
 }
 sprintf(fname,"0%i02.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++) img[i]=1;
  for(i=0;i<8;i++){img[24+i]=3;img[32+i]=3;}
 }
 else
 {
  for(i=0;i<64;i++) img[i]=h;
 }
 sprintf(fname,"0%i04.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[18]=2;img[19]=2;img[20]=2;img[21]=2;
  img[26]=2;img[27]=0;img[28]=0;img[29]=2;
  img[34]=2;img[35]=0;img[36]=0;img[37]=2;
  img[42]=2;img[43]=2;img[44]=2;img[45]=2;
  img[ 3]=1;img[ 4]=1;
  img[11]=1;img[12]=1;
  img[24]=3;img[25]=3;
  img[32]=3;img[33]=3;
 }
 else
 {
  img[18]=h;img[19]=h;img[20]=h;img[21]=h;
  img[26]=h;img[27]=0;img[28]=0;img[29]=h;
  img[34]=h;img[35]=0;img[36]=0;img[37]=h;
  img[42]=h;img[43]=h;img[44]=h;img[45]=h;
  img[ 3]=h;img[ 4]=h;
  img[11]=h;img[12]=h;
  img[24]=h;img[25]=h;
  img[32]=h;img[33]=h;
 }
 sprintf(fname,"0%i05.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[30]=3;img[31]=3;
  img[38]=3;img[39]=3;
 }
 else
 {
  img[30]=h;img[31]=h;
  img[38]=h;img[39]=h;
 }
 sprintf(fname,"0%i07.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 img[24]=0;img[25]=0;
 img[32]=0;img[33]=0;
 sprintf(fname,"0%i06.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[24]=1;img[32]=1;img[40]=1;
  img[33]=1;img[41]=1;img[49]=1;
  img[42]=1;img[50]=1;img[58]=1;
  img[51]=1;img[59]=1;
  img[60]=1;
 }
 else
 {
  img[24]=h;img[32]=h;img[40]=h;
  img[33]=h;img[41]=h;img[49]=h;
  img[42]=h;img[50]=h;img[58]=h;
  img[51]=h;img[59]=h;
  img[60]=h;
 }
 sprintf(fname,"0%i08.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[18]=2;img[19]=2;img[20]=2;img[21]=2;
  img[26]=2;img[27]=0;img[28]=0;img[29]=2;
  img[34]=2;img[35]=0;img[36]=0;img[37]=2;
  img[42]=2;img[43]=2;img[44]=2;img[45]=2;
  img[51]=1;img[52]=1;
  img[59]=1;img[60]=1;
  img[24]=3;img[25]=3;
  img[32]=3;img[33]=3;
 }
 else
 {
  img[18]=h;img[19]=h;img[20]=h;img[21]=h;
  img[26]=h;img[27]=0;img[28]=0;img[29]=h;
  img[34]=h;img[35]=0;img[36]=0;img[37]=h;
  img[42]=h;img[43]=h;img[44]=h;img[45]=h;
  img[51]=h;img[52]=h;
  img[59]=h;img[60]=h;
  img[24]=h;img[25]=h;
  img[32]=h;img[33]=h;
 }
 sprintf(fname,"0%i09.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[30]=3;img[31]=3;
  img[38]=3;img[39]=3;
 }
 else
 {
  img[30]=h;img[31]=h;
  img[38]=h;img[39]=h;
 }
 sprintf(fname,"0%i0b.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 img[24]=0;img[25]=0;
 img[32]=0;img[33]=0;
 sprintf(fname,"0%i0a.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[3]=1;img[4]=1;img[5]=1;
  img[12]=1;img[13]=1;img[14]=1;
  img[21]=1;img[22]=1;img[23]=1;
  img[30]=1;img[31]=1;
  img[39]=1;
 }
 else
 {
  img[3]=h;img[4]=h;img[5]=h;
  img[12]=h;img[13]=h;img[14]=h;
  img[21]=h;img[22]=h;img[23]=h;
  img[30]=h;img[31]=h;
  img[39]=h;
 }
 sprintf(fname,"0%i0c.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[18]=2;img[19]=2;img[20]=2;img[21]=2;
  img[26]=2;img[27]=0;img[28]=0;img[29]=2;
  img[34]=2;img[35]=0;img[36]=0;img[37]=2;
  img[42]=2;img[43]=2;img[44]=2;img[45]=2;
  img[ 3]=1;img[ 4]=1;
  img[11]=1;img[12]=1;
  img[24]=3;img[25]=3;
  img[32]=3;img[33]=3;
  img[51]=1;img[52]=1;
  img[59]=1;img[60]=1;
 }
 else
 {
  img[18]=h;img[19]=h;img[20]=h;img[21]=h;
  img[26]=h;img[27]=0;img[28]=0;img[29]=h;
  img[34]=h;img[35]=0;img[36]=0;img[37]=h;
  img[42]=h;img[43]=h;img[44]=h;img[45]=h;
  img[ 3]=h;img[ 4]=h;
  img[11]=h;img[12]=h;
  img[24]=h;img[25]=h;
  img[32]=h;img[33]=h;
  img[51]=h;img[52]=h;
  img[59]=h;img[60]=h;
 }
 sprintf(fname,"0%i0d.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[30]=3;img[31]=3;
  img[38]=3;img[39]=3;
 }
 else
 {
  img[30]=h;img[31]=h;
  img[38]=h;img[39]=h;
 }
 sprintf(fname,"0%i0f.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 img[24]=0;img[25]=0;
 img[32]=0;img[33]=0;
 sprintf(fname,"0%i0e.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++) img[i]=1;
 }
 else
 {
  for(i=0;i<64;i++) img[i]=h;
 }
 sprintf(fname,"0%i10.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  for(i=0;i<8;i++){img[24+i]=3;img[32+i]=3;}
 }
 else
 {
  for(i=0;i<8;i++){img[24+i]=h;img[32+i]=h;}
 }
 sprintf(fname,"0%i11.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  for(i=0;i<8;i++){img[3+i*8]=1;img[4+i*8]=1;}
 }
 else
 {
  for(i=0;i<8;i++){img[3+i*8]=h;img[4+i*8]=h;}
 }
 sprintf(fname,"0%i12.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<8;i++) img[24+i]=3;
  for(i=0;i<8;i++) img[32+i]=3;
  img[18]=0;img[19]=1;img[20]=1;img[21]=0;
  img[42]=0;img[43]=1;img[44]=1;img[45]=0;
 }
 else
 {
  for(i=0;i<8;i++) img[24+i]=h;
  for(i=0;i<8;i++) img[32+i]=h;
  img[18]=0;img[19]=h;img[20]=h;img[21]=0;
  img[42]=0;img[43]=h;img[44]=h;img[45]=0;
 }
 sprintf(fname,"0%i13.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  for(i=0;i<8;i++){img[6+i*8]=2;img[7+i*8]=2;}
  for(i=1;i<7;i++){img[4+i*8]=2;img[5+i*8]=2;}
  for(i=2;i<6;i++) img[3+i*8]=2;
 }
 else
 {
  for(i=0;i<8;i++){img[6+i*8]=h;img[7+i*8]=h;}
  for(i=1;i<7;i++){img[4+i*8]=h;img[5+i*8]=h;}
  for(i=2;i<6;i++) img[3+i*8]=h;
 }
 sprintf(fname,"0%i14.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[24]=3;img[25]=3;img[26]=3;
  img[32]=3;img[33]=3;img[34]=3;
 }
 else
 {
  img[24]=h;img[25]=h;img[26]=h;
  img[32]=h;img[33]=h;img[34]=h;
 }
 sprintf(fname,"0%i15.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  for(i=0;i<8;i++){img[i*8]=2;img[1+i*8]=2;}
  for(i=1;i<7;i++){img[2+i*8]=2;img[3+i*8]=2;}
  for(i=2;i<6;i++) img[4+i*8]=2;
 }
 else
 {
  for(i=0;i<8;i++){img[i*8]=h;img[1+i*8]=h;}
  for(i=1;i<7;i++){img[2+i*8]=h;img[3+i*8]=h;}
  for(i=2;i<6;i++) img[4+i*8]=h;
 }
 sprintf(fname,"0%i16.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  img[29]=3;img[30]=3;img[31]=3;
  img[37]=3;img[38]=3;img[39]=3;
 }
 else
 {
  img[29]=h;img[30]=h;img[31]=h;
  img[37]=h;img[38]=h;img[39]=h;
 }
 sprintf(fname,"0%i17.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++) img[i]=2;
 }
 else
 {
  for(i=0;i<64;i++) img[i]=h;
 }
 img[19]=0;img[20]=0;
 img[26]=0;img[27]=0;img[28]=0;img[29]=0;
 img[34]=0;img[35]=0;img[36]=0;img[37]=0;
 img[43]=0;img[44]=0;
 sprintf(fname,"0%i18.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 img[0]=0;img[1]=0;img[8]=0;
 img[6]=0;img[7]=0;img[15]=0;
 img[56]=0;img[57]=0;img[48]=0;
 img[62]=0;img[63]=0;img[55]=0;
 sprintf(fname,"0%i19.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++)
  {
   if(i>=16 && i<48) img[i]=3;
   else img[i]=0;
  }
 }
 else
 {
  for(i=0;i<64;i++)
  {
   if(i>=16 && i<48) img[i]=h;
   else img[i]=0;
  }
 }
 sprintf(fname,"0%i1a.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 if(h==0||h==4)
 {
  for(i=0;i<64;i++) img[i]=3;
 }
 else
 {
  for(i=0;i<64;i++) img[i]=h;
 }
 sprintf(fname,"0%i1b.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[16]=3;img[24]=3;img[32]=3;
  img[9]=3;img[17]=3;img[25]=3;
  img[2]=3;img[10]=3;img[18]=3;
  img[3]=3;img[11]=3;
  img[4]=3;
 }
 else
 {
  img[16]=h;img[24]=h;img[32]=h;
  img[9]=h;img[17]=h;img[25]=h;
  img[2]=h;img[10]=h;img[18]=h;
  img[3]=h;img[11]=h;
  img[4]=h;
 }
 sprintf(fname,"0%i1c.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[31]=3;img[39]=3;img[47]=3;
  img[38]=3;img[46]=3;img[54]=3;
  img[45]=3;img[53]=3;img[61]=3;
  img[52]=3;img[60]=3;
  img[59]=3;
 }
 else
 {
  img[31]=h;img[39]=h;img[47]=h;
  img[38]=h;img[46]=h;img[54]=h;
  img[45]=h;img[53]=h;img[61]=h;
  img[52]=h;img[60]=h;
  img[59]=h;
 }
 sprintf(fname,"0%i1d.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[4]=2;
  img[12]=2;
  img[19]=2;img[20]=2;img[21]=2;
  img[27]=2;img[28]=2;img[29]=2;
  img[34]=2;img[35]=2;img[36]=2;img[37]=2;img[38]=2;
  img[42]=2;img[43]=2;img[44]=2;img[45]=2;img[46]=2;
  img[49]=2;img[50]=2;img[52]=2;img[54]=2;img[55]=2;
  img[57]=2;img[60]=2;img[63]=2;
 }
 else
 {
  img[4]=h;
  img[12]=h;
  img[19]=h;img[20]=h;img[21]=h;
  img[27]=h;img[28]=h;img[29]=h;
  img[34]=h;img[35]=h;img[36]=h;img[37]=h;img[38]=h;
  img[42]=h;img[43]=h;img[44]=h;img[45]=h;img[46]=h;
  img[49]=h;img[50]=h;img[52]=h;img[54]=h;img[55]=h;
  img[57]=h;img[60]=h;img[63]=h;
 }
 sprintf(fname,"0%i1e.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

 for(i=0;i<64;i++) img[i]=0;
 if(h==0||h==4)
 {
  img[60]=2;
  img[52]=2;
  img[43]=2;img[44]=2;img[45]=2;
  img[35]=2;img[36]=2;img[37]=2;
  img[26]=2;img[27]=2;img[28]=2;img[29]=2;img[30]=2;
  img[18]=2;img[19]=2;img[20]=2;img[21]=2;img[22]=2;
  img[9]=2;img[10]=2;img[12]=2;img[14]=2;img[15]=2;
  img[1]=2;img[4]=2;img[7]=2;
 }
 else
 {
  img[60]=h;
  img[52]=h;
  img[43]=h;img[44]=h;img[45]=h;
  img[35]=h;img[36]=h;img[37]=h;
  img[26]=h;img[27]=h;img[28]=h;img[29]=h;img[30]=h;
  img[18]=h;img[19]=h;img[20]=h;img[21]=h;img[22]=h;
  img[9]=h;img[10]=h;img[12]=h;img[14]=h;img[15]=h;
  img[1]=h;img[4]=h;img[7]=h;
 }
 sprintf(fname,"0%i1f.gif",h);
 SaveGif(fname,img,colormap,4,8,8);

}

#endif

return 0;
}
