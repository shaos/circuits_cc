/* gifs2c.c - convert set of gifs for Cirtcuis.CC to C array */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PSZ 64

#define ISZ 0x500

int main(int argc, char **argv)
{
 int i,j,k;
 char s[32];
 unsigned char r[PSZ];
 unsigned char g[PSZ];
 unsigned char b[PSZ];
 FILE *f,*fo;
 if(argc<2)
 {
   printf("\nUsage:\n\tgifs2c output.c\n");
   return -1;
 }
 fo = fopen(argv[1],"wt");
 if(fo==NULL) return -2;
 fprintf(fo,"long pcbimg[%i] = {\n",ISZ*PSZ);
 for(i=0;i<ISZ;i++)
 {
   sprintf(s,"gif2rgb -o 0 %04x.gif",i);
   printf("[%i] -> convert '%s'\n",i,s);
   system(s);
   f = fopen("0.R","rb");
   if(f==NULL) break;
   fread(r,PSZ,1,f);
   fclose(f);
   f = fopen("0.G","rb");
   if(f==NULL) break;
   fread(g,PSZ,1,f);
   fclose(f);
   f = fopen("0.B","rb");
   if(f==NULL) break;
   fread(b,PSZ,1,f);
   fclose(f);
   fprintf(fo," /* 0x%02X */\n",i);
   for(j=0;j<PSZ;j++)
   {
      fprintf(fo," 0x%02X%02X%02X,",r[j],g[j],b[j]);
      if((j&7)==7) fprintf(fo,"\n");
   }
 }
 if(i!=ISZ) printf("\nERROR!\n");
 fprintf(fo,"};\n");
 fclose(fo);
 return 0;
}

