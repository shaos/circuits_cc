/*
   Convert history file from old Circuits.CC to PNG sequence for video
*/

#define PNG
#define MAGNIFY
#define DATETIMEIP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef PNG
#include <png.h>
#include "pcbimgs.c"
#endif

#define DX 250
#define DY 136

unsigned char scr[DX][DY];

#define XOFF 80
#define YOFF 47

#ifdef PNG

#define PNGXOFF 10
#define PNGYOFF 1

char ClickID[6] = "1    ";

char DateTime[20] = "                ";

char Activity[256];

unsigned int CurTime = 0;

struct ipstat
{
  unsigned char ip[4];
  unsigned int ti,ac;
} ips[64];

int getscr(int f, int x, int y)
{
 int c = scr[x][y];
/*
f < 135
CIRCUITS.CC      SANDBOX -> x=37              80
================================================
\0123456789ABCDEF (white)

f < 499 
switch some characters

f >= 135 && f < 5435 ?
CIRCUITS.CC      SANDBOX -> x=47              100
=================================================
\0123456789ABCDEF (white)

f > 5435 ?
 0123456789ABCDEF Circuits.CC v0.2 -> x=42   100
0                | (inverted)
*/
 int w80 = 135;
 int mor = 5254;
 int v02 = 5435;
 if(f<468) switch(c)
 {
   case 0xB2: c=0xBA; break;
   case 0x04: c=0x0F; break;
   case 0x08: c=0x0F; break;
   case 0x0C: c=0x0F; break;
   case 0x1B: c=0x04; break;
   case 0x1C: c=0x10; break;
   case 0x1D: c=0x01; break;
   case 0x1E: c=0x02; break;
   case 0x1F: c=0x03; break;
 }
 if(f < mor) // v02
 {
   if(f < w80)
   {
     if(y<YOFF || y>=YOFF+43 || x<XOFF || x>=XOFF+80) c=' ';
   }
   else if(f<mor)
   {
     if(y<YOFF || y>=YOFF+75 || x<XOFF || x>=XOFF+100) c=' ';
   }
   if(y==YOFF)
   {
     switch(x-XOFF)
     {
      case 0: c='C'; break;
      case 1: c='I'; break;
      case 2: c='R'; break;
      case 3: c='C'; break;
      case 4: c='U'; break;
      case 5: c='I'; break;
      case 6: c='T'; break;
      case 7: c='S'; break;
      case 8: c='.'; break;
      case 9: c='C'; break;
      case 10: c='C'; break;
      case 37: if(f<w80) c='S'; else c=' '; break;
      case 38: if(f<w80) c='A'; else c=' '; break;
      case 39: if(f<w80) c='N'; else c=' '; break;
      case 40: if(f<w80) c='D'; else c=' '; break;
      case 41: if(f<w80) c='B'; else c=' '; break;
      case 42: if(f<w80) c='O'; else c=' '; break;
      case 43: if(f<w80) c='X'; else c=' '; break;
      case 47: if(f>=w80) c='S'; else c=' '; break;
      case 48: if(f>=w80) c='A'; else c=' '; break;
      case 49: if(f>=w80) c='N'; else c=' '; break;
      case 50: if(f>=w80) c='D'; else c=' '; break;
      case 51: if(f>=w80) c='B'; else c=' '; break;
      case 52: if(f>=w80) c='O'; else c=' '; break;
      case 53: if(f>=w80) c='X'; else c=' '; break;
      case 75: if(f<w80) c=ClickID[4]; else c=' '; break;
      case 76: if(f<w80) c=ClickID[3]; else c=' '; break;
      case 77: if(f<w80) c=ClickID[2]; else c=' '; break;
      case 78: if(f<w80) c=ClickID[1]; else c=' '; break;
      case 79: if(f<w80) c=ClickID[0]; else c=' '; break;
      case 95: if(f>=w80) c=ClickID[4]; else c=' '; break;
      case 96: if(f>=w80) c=ClickID[3]; else c=' '; break;
      case 97: if(f>=w80) c=ClickID[2]; else c=' '; break;
      case 98: if(f>=w80) c=ClickID[1]; else c=' '; break;
      case 99: if(f>=w80) c=ClickID[0]; else c=' '; break;
      default: c=' '; break;
     }
     c += 0x100;
   }
   if(y==YOFF+1 && x>=XOFF && x<XOFF+((f<w80)?80:100)) c=0x1CD;
   if(y==YOFF+2 && x==XOFF) c=0x100+'\\';
   if(y==YOFF+2 && x>XOFF && x<=XOFF+16) c=(x<=XOFF+10)?(0x100+'/'+x-XOFF):(0x100+'A'+x-XOFF-11);
   if(x==XOFF && y>YOFF+2 && y<=YOFF+18) c=(y<=YOFF+12)?(0x100+'/'+y-YOFF-2):(0x100+'A'+y-YOFF-13);
   if(x>XOFF && x<=XOFF+16 && y>YOFF+2 && y<=YOFF+18)
   {
     c = (x-XOFF-1)+(y-YOFF-3)*16;
     if(Activity[c]) c = 0x310;
   }
 }
 else if(c==0) c=0x20;
 return c;
}

#ifdef MAGNIFY
double xx1 = 320;
double yy1 = 308;
double dy1 = 720;
double dy2 = 1080;
int fr1 = 4123;
int fr2 = 5827;
#endif

int savepng(int fr)
{
 char str[32];
 FILE *f;
 int x,y,ix,iy,it,ic;
 int ix1,ix2,iy1,iy2;
 double xd,yd;
 sprintf(str,"frames/%06d.png",fr);
 printf("Save '%s'\n",str);
 f = fopen(str, "wb");
 if(!f) return -3;
 png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
 if(!png_ptr) return -4;
 png_infop info_ptr = png_create_info_struct(png_ptr);
 if(!info_ptr) return -5;
 if(setjmp(png_jmpbuf(png_ptr))) return -6;
 png_init_io(png_ptr, f);
 if(setjmp(png_jmpbuf(png_ptr))) return -7;
 png_set_IHDR(png_ptr, info_ptr, 1920, 1080, 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
 png_write_info(png_ptr, info_ptr);
 if(setjmp(png_jmpbuf(png_ptr))) return -8;
 png_bytep* row_pointers = (png_bytep*) malloc(sizeof(png_bytep)*1080);
 if(row_pointers==NULL) return -9;
#ifdef MAGNIFY
 png_bytep row1 = (png_byte*)malloc(png_get_rowbytes(png_ptr,info_ptr));
 png_bytep row2 = (png_byte*)malloc(png_get_rowbytes(png_ptr,info_ptr));
#endif
 for(y=0;y<1080;y++)
 {
    row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png_ptr,info_ptr));
    if(row_pointers[y]==NULL) return -10;
    iy = (y>>3)+PNGYOFF;
    for(x=0;x<1920;x++)
    {
      ix = (x>>3)+PNGXOFF;
      if(!(x&7))
      {
         if(ix < 0 || ix >= DX || iy < 0 || iy >= DY) ic = 0;
         else ic = getscr(fr,ix,iy);
      }
      it = pcbimg[(ic<<6) + (y&7)*8 + (x&7)];
      row_pointers[y][(x<<2)+0] = (it>>16)&255;
      row_pointers[y][(x<<2)+1] = (it>>8)&255;
      row_pointers[y][(x<<2)+2] = it&255;
      row_pointers[y][(x<<2)+3] = 255;
    }
 }
#ifdef MAGNIFY
 double sca = dy2/dy1;
 double xx0 = 0;
 double yy0 = 0;
 if(fr < fr1)
 {
   xx0 = xx1;
   yy0 = yy1;
 }
 else
 {
   double frs = ((double)(fr-fr1))/(fr2-fr1);
   xx0 = xx1-xx1*frs;
   yy0 = yy1-yy1*frs;
   sca = frs + dy2/dy1*(1-frs); /* dy2/dy1 -> 1.0 */
 }
 if(fr < fr2) // no magnification after fr2
 {
  int ycut = 1080;
  for(y=0;y<1080;y++)
  {
    int yy = y;
    if(y > ycut) yy = 1079-(y-ycut);
    double yy2 = yy/sca+yy0;
    if(ycut==1080 && yy2 <= (double)y)
    {
       ycut = y;
       yy = 1079-(y-ycut);
       yy2 = yy/sca+yy0;
    }
    int yyy = (int)yy2;
    int yyyy = yyy+1;
    double nei2 = yy2-yyy;
    double nei1 = 1.0-nei2;
//    printf("%i %f %i %2.2f %i %2.2f\n",yy,yy2,yyy,nei1,yyyy,nei2);
    memcpy(row1,row_pointers[yyy],png_get_rowbytes(png_ptr,info_ptr));
    memcpy(row2,row_pointers[yyyy],png_get_rowbytes(png_ptr,info_ptr));
    for(x=0;x<1920;x++)
    {
       double xx2 = x/sca+xx0;
       int xxx = (int)xx2;
       int xxxx = xxx+1;
       double xnei2 = xx2-xxx;
       double xnei1 = 1.0-xnei2;
//       printf(">>> %i %f %i %2.2f %i %2.2f\n",x,xx2,xxx,xnei1,xxxx,xnei2);
       for(int ii=0;ii<4;ii++)
       {
          double cc1 = row1[(xxx<<2)+ii]*xnei1 + row1[(xxxx<<2)+ii]*xnei2;
          double cc2 = row2[(xxx<<2)+ii]*xnei1 + row2[(xxxx<<2)+ii]*xnei2;
          int cc = cc1*nei1 + cc2*nei2;
          if(cc < 0) cc = 0;
          if(cc > 255) cc = 255;
          row_pointers[yy][(x<<2)+ii] = cc;
       }
    }
  }
 }
#endif
#ifdef DATETIMEIP
 if(*DateTime!=' ')
 {
#if 0
   for(y=0;y<16;y++)
   {
    for(x=1648;x<1904;x++)
    {
      ix = (x-1648)>>4;
      if(!(x&15)) ic = DateTime[ix] + 0x300;
      it = pcbimg[(ic<<6) + ((y>>1)&7)*8 + ((x&15)>>1)];
      row_pointers[y+16][(x<<2)+0] = (it>>16)&255;
      row_pointers[y+16][(x<<2)+1] = (it>>8)&255;
      row_pointers[y+16][(x<<2)+2] = it&255;
      row_pointers[y+16][(x<<2)+3] = 255;
    }
   }
#else
   for(y=0;y<32;y++)
   {
    for(x=1376;x<1888;x++)
    {
      ix = (x-1376)>>5;
      if(!(x&31)) ic = DateTime[ix] + 0x300;
      it = pcbimg[(ic<<6) + ((y>>2)&7)*8 + ((x&31)>>2)];
      row_pointers[y+32][(x<<2)+0] = (it>>16)&255;
      row_pointers[y+32][(x<<2)+1] = (it>>8)&255;
      row_pointers[y+32][(x<<2)+2] = it&255;
      row_pointers[y+32][(x<<2)+3] = 255;
    }
   }
#endif
 }
 for(y=32;y<1056;y++)
 {
    iy = 63-((y-32)>>4);
    if(!ips[iy].ti) continue;
    else sprintf(str,"%i.%i.%i.%i          ",ips[iy].ip[0],ips[iy].ip[1],ips[iy].ip[2],ips[iy].ip[3]);
    for(x=16;x<272;x++)
    {
      ix = (x-16)>>4;
      if(!(x&15))
      {
         ic = str[ix] + (ips[iy].ac?(ips[iy].ac>=256?0x200:0x300):0x100);
      }
      it = pcbimg[(ic<<6) + ((y>>1)&7)*8 + ((x&15)>>1)];
      row_pointers[y][(x<<2)+0] = (it>>16)&255;
      row_pointers[y][(x<<2)+1] = (it>>8)&255;
      row_pointers[y][(x<<2)+2] = it&255;
      row_pointers[y][(x<<2)+3] = 255;
    }
 }
#endif
 png_write_image(png_ptr, row_pointers);
 if(setjmp(png_jmpbuf(png_ptr))) return -11;
 png_write_end(png_ptr, NULL);
 for(y=0;y<1080;y++) free(row_pointers[y]);
 free(row_pointers);
#ifdef MAGNIFY
 free(row1);
 free(row2);
#endif
 fclose(f);
 return 0;
}

#endif

int main()
{
 char str[100],st[50],stt[50],sto[50],*po;
 unsigned int it;
 int x,y,ix,iy,ic,ox,oy,oc,ii,ff,i1,i2,i3,i4,jj,jjj,iii,kkk,iip,mip=0;
 int frames = 0;
 int counter = 0;
 int xmin = DX;
 int ymin = DY;
 int xmax = -DX;
 int ymax = -DY;
 for(y=0;y<DY;y++){
 for(x=0;x<DX;x++){
   scr[x][y] = 0;
 }}
 for(ii=0;ii<64;ii++) ips[ii].ti = 0;
 for(ii=0;ii<256;ii++) Activity[ii] = 0;
 FILE* f = fopen("pcb0allt.out","rt");
 if(f==NULL) return -1;
 *sto = ox = oy = 0;
#ifdef PNG
 savepng(frames++);
#endif
 while(1)
 {
   fgets(str,100,f);
   if(feof(f)) break;
   sscanf(str,"%i|%i|%i|%i|%i.%i.%i.%i|%u|%s %s\n",&ii,&ix,&iy,&ic,&i1,&i2,&i3,&i4,&it,st,stt);
   if(!strncmp(st,"1969",4))
   {
     *st = 0;
     *stt = 0;
   }
   else
   {
     po = strchr(stt,':');
     if(po!=NULL)
     {
      po = strchr(++po,':');
      if(po!=NULL) *po = 0;
     }
   }
   ff = 0;
   if(ix < xmin) { xmin = ix; ff++; }
   if(ix > xmax) { xmax = ix; ff++; }
   if(iy < ymin) { ymin = iy; ff++; }
   if(iy > ymax) { ymax = iy; ff++; }
#if 1
   if(ff) printf("%i|%s|%s| x=%i..%i y=%i..%i\n",ii,st,stt,xmin,xmax,ymin,ymax);
#endif
   sprintf(DateTime,"%s %s",st,stt);
   Activity[ic&255]++;
#if 1
   for(jj=0;jj<64;jj++)
   {
      if(ips[jj].ti && ips[jj].ti < it-60*60*24) /* 1 day */
         ips[jj].ti = 0;
   }
   iip = 0;
   jjj = -1;
   for(jj=0;jj<64;jj++)
   {
      if(ips[jj].ti)
      {
         ips[iip].ti = ips[jj].ti;
         ips[iip].ip[0] = ips[jj].ip[0];
         ips[iip].ip[1] = ips[jj].ip[1];
         ips[iip].ip[2] = ips[jj].ip[2];
         ips[iip].ip[3] = ips[jj].ip[3];
         ips[iip].ac = ips[jj].ac;
         if(iip!=jj) ips[jj].ti = 0;
         if(ips[iip].ip[0]==i1 && ips[iip].ip[1]==i2 && ips[iip].ip[2]==i3 && ips[iip].ip[3]==i4) jjj=iip;
         iip++;
      }
   }
   if(jjj < 0)
   {
      ips[iip].ti = it;
      ips[iip].ip[0] = i1;
      ips[iip].ip[1] = i2;
      ips[iip].ip[2] = i3;
      ips[iip].ip[3] = i4;
      ips[iip].ac = (ix||iy)?1:0;
      iip++;
   }
   else
   {
      ips[jjj].ti = it;
      ips[jjj].ac += (ix||iy)?1:0;
   }
   if(iip > mip) mip = iip;
#endif
   iii = ii;
   kkk = 10000;
   for(jj=4;jj>=0;jj--)
   {
     jjj = iii/kkk;
     if(jjj > 0 || ClickID[jj]!=' ') ClickID[jj] = jjj + '0';
     iii -= jjj*kkk;
     kkk /= 10;
   }
   jj = ox - ix;
   if(jj < 0) jj=-jj;
   if(jj > 1)
   {
     jj = oy - iy;
     if(jj < 0) jj=-jj;
   }
   x = ix + XOFF;
   y = iy + YOFF;
   if(x<0) x=0;
   if(x>=DX) x=DX-1;
   if(y<0) y=0;
   if(y>=DY) y=DY-1;
   oc = scr[x][y];
   scr[x][y] = ic;
   if((ic==0 || ic==0x20 || ic==oc || jj <= 1 || (*stt && !strcmp(stt,sto))) && counter < 32) counter++;
   else
   {
      printf("Frame %i (%i) - %i changes and %i addresses (%i max)\n",frames,ii,counter,iip,mip);
      counter = 1;
#ifdef PNG
//      if(frames>5010)
      savepng(frames);
#endif
      ++frames;
      for(ii=0;ii<256;ii++) Activity[ii] = 0;
   }
   ox = ix;
   oy = iy;
   strcpy(sto,stt);
 }
 fclose(f);
 return 0;
}

