<?php
/*

This is simple server side PHP-script for Circuits.CC v0.3 created by Shaos in May 2019

You need to create a database and a table in your MySQL instance before using it:

CREATE TABLE pcb_action (
  id int unsigned primary key not null auto_increment,
  col smallint not null,
  row smallint not null,
  sym smallint not null,
  flag binary not null default 0,
  utime int unsigned not null default 0,
  adr varchar(16) not null default '127.0.0.1',
  usr varchar(20),
  index pcbi_crf (col,row,flag),
  index pcbi_au (usr,adr,utime)
) ENGINE=InnoDB;

*/
$con = mysql_connect("localhost","shaos","xxxxx"); // use your server, username and password
mysql_select_db("nedopc",$con); // use your database name here
$res = false;
$id = (int)$_GET["id"];
if($id==0)
{
  $res = mysql_query("select pcb1.col,pcb1.row,pcb1.sym,pcb1.id from pcb_action as pcb1 inner join
                     (select col,row,max(id) as maxid from pcb_action where flag=0 group by col,row) as pcb2
                      on pcb1.col=pcb2.col and pcb1.row=pcb2.row and pcb1.id=maxid",$con);
}
else
{
  $uni = time();
  $col = (int)$_GET["col"];
  $row = (int)$_GET["row"];
  $sym = (int)$_GET["sym"];
  $adr = $_SERVER["REMOTE_ADDR"];
  $query = sprintf("insert into pcb_action (col,row,sym,adr,utime) values (%d,%d,%d,\"%s\",%u)",$col,$row,$sym,$adr,$uni);
  $res = mysql_query($query) or die("ERROR");
  $query = sprintf("select col,row,sym,id from pcb_action where id>%d",$id);
  $res = mysql_query($query);
}
while($rec = mysql_fetch_array($res))
{
  print "{$rec["col"]},{$rec["row"]},{$rec["sym"]},{$rec["id"]}\n";
}

?>
